# Trainable Entry Layer in Fourier Domain at top of ConvNets  Improves the  Robustness to large Brightness Changes in Image classification

Although Convolutional Neural Networks (ConvNets) can currently achieve remarkable performance in computer vision tasks, the conventional ConvNets cannot guarantee this performance even with small changes in input images, for example, with brightness variations. In this paper we present an analysis of the classification performance of a trainable entry layer in the Fourier domain (M6) capable of classifying images even with large alterations of  their brightness. With only two trainable parameters and multi-channel processing, this layer is an improved version of an earlier quaternion monogenic layer. We compare the performance of five ConvNets architectures topped by either a conventional convolutional layer (C) or an M6 layer. In addition, we use four datasets with four levels of brightness (artificially generated). The numerical results in the test sets confirm that the M6 local phases' maps are surprisingly stable in front of large brightness variations.

## Requirements

To install requirements:

```setup
conda env create -f environment.yml
```

Creates a conda environment called `monogenic`. You can access this with the following command:

```condaenv
conda activate monogenic
```


## Training and evaluation

To train the model(s), run this command:

```train
python main.py --output-path <path_to_data> --epochs 100 --learning-rate 0.0001 --batch-size 128
```
> example: python main.py --output-path results --epochs 100 --learning-rate 0.0001 --batch-size 128

- The script creates the `<path_to_data>` directory where the training results are stored.
- The script downloads the datasets MNIST, Fashion-MNIST, CIFAR-10 and Cats vs dogs through [TensorFlow dataset](https://www.tensorflow.org/datasets/catalog/overview) package.


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

```license
MIT License

Copyright (c) 2020 Abraham Sánchez <abraham.sanchez@jalisco.gob.mx>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

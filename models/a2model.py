#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model

from models.constants import ArchitectureTests


def get_model(input_shape, num_classes, data_name, m6_layer=False):
    inputs = Input(input_shape)
    f = 6
    if data_name in ArchitectureTests.SMALL_SIZE_DATASETS:
        from layers.m6 import M6
    else:
        from layers.m18 import M6
        f = 18
    if m6_layer:
        x = M6()(inputs)
    else:
        x = Conv2D(f, (3, 3), padding='same', activation='relu')(inputs)
    x = Conv2D(f, (3, 3), padding='same', activation='relu')(x)
    x = Conv2D(f, (3, 3), padding='same', activation='relu')(x)
    x = Flatten()(x)
    x = Dense(256, activation='relu')(x)
    x = Dense(128, activation='relu')(x)
    x = Dense(num_classes, activation='softmax')(x)
    model = Model(inputs=inputs, outputs=x)
    return model

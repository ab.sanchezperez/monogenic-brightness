#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.applications.inception_v3 import InceptionV3

from models.constants import ArchitectureTests


def get_model(input_shape, num_classes, data_name, m6_layer=False):
    inputs = Input(input_shape)
    if data_name in ArchitectureTests.SMALL_SIZE_DATASETS:
        from layers.m6 import M6
    else:
        from layers.m18 import M6
    if m6_layer:
        m6output = M6()(inputs)
        m6model = Model(inputs=inputs, outputs=m6output)
        inception = InceptionV3(input_tensor=m6model.output, weights=None, include_top=False)
    else:
        inception = InceptionV3(input_tensor=inputs, weights=None, include_top=False)
    x = inception.output
    x = Flatten()(x)
    x = Dense(units=num_classes, activation='softmax')(x)
    model = Model(inputs=inputs, outputs=x)
    return model

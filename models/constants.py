#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


class Architectures:
    A1C_ARCH = 'A1_C'
    A2C_ARCH = 'A2_C'
    A1M6_ARCH = 'A1_M6'
    A2M6_ARCH = 'A2_M6'
    A3C_ARCH = 'A3_C'
    A3M6_ARCH = 'A3_M6'
    A4C_ARCH = 'A4_C'
    A4M6_ARCH = 'A4_M6'
    A5C_ARCH = 'A5_C'
    A5M6_ARCH = 'A5_M6'


class ArchitectureTests:
    # List of mid size datasets
    SMALL_SIZE_DATASETS = ['mnist', 'fashion_mnist']
    # List of mid size datasets
    MID_SIZE_DATASETS = ['cifar10']
    # List of large size datasets
    LARGE_SIZE_DATASETS = ['cats_vs_dogs']
    # List of available architectures for small size datasets
    AVAIL_SMALL_ARCHS = [Architectures.A1C_ARCH, Architectures.A1M6_ARCH,
                         Architectures.A2C_ARCH, Architectures.A2M6_ARCH]
    # List of available architectures for mid size datasets
    AVAIL_MID_ARCHS = AVAIL_SMALL_ARCHS.copy()
    AVAIL_MID_ARCHS.extend([
        Architectures.A3C_ARCH, Architectures.A3M6_ARCH
    ])
    # List of available architectures for large size datasets
    AVAIL_LARGE_ARCHS = AVAIL_MID_ARCHS.copy()
    AVAIL_LARGE_ARCHS.extend([
        Architectures.A4C_ARCH, Architectures.A4M6_ARCH,
        Architectures.A5C_ARCH, Architectures.A5M6_ARCH
    ])
    ARCH_TYPES = [
        Architectures.A1C_ARCH,
        Architectures.A1M6_ARCH,
        Architectures.A2C_ARCH,
        Architectures.A2M6_ARCH,
        Architectures.A3C_ARCH,
        Architectures.A3M6_ARCH,
        Architectures.A4C_ARCH,
        Architectures.A4M6_ARCH,
        Architectures.A5C_ARCH,
        Architectures.A5M6_ARCH
    ]

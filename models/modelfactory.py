#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import models.a1model
import models.a2model
import models.a3model
import models.a4model
import models.a5model

from models.constants import Architectures


class ModelFactory(Architectures):

    @staticmethod
    def instance(arch_type, input_shape, num_classes, data_name):
        if arch_type == Architectures.A1C_ARCH:
            return models.a1model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name)
        elif arch_type == Architectures.A1M6_ARCH:
            return models.a1model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name,
                                            m6_layer=True)
        elif arch_type == Architectures.A2C_ARCH:
            return models.a2model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name)
        elif arch_type == Architectures.A2M6_ARCH:
            return models.a2model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name,
                                            m6_layer=True)
        elif arch_type == Architectures.A3C_ARCH:
            return models.a3model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name)
        elif arch_type == Architectures.A3M6_ARCH:
            return models.a3model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name,
                                            m6_layer=True)
        elif arch_type == Architectures.A4C_ARCH:
            return models.a4model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name)
        elif arch_type == Architectures.A4M6_ARCH:
            return models.a4model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name,
                                            m6_layer=True)
        elif arch_type == Architectures.A5C_ARCH:
            return models.a5model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name)
        elif arch_type == Architectures.A5M6_ARCH:
            return models.a5model.get_model(input_shape=input_shape, num_classes=num_classes, data_name=data_name,
                                            m6_layer=True)

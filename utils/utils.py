#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import os


def create_directory(dir_name):
    try:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
    except IOError:
        raise

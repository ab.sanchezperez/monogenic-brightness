#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import pandas as pd
import tensorflow as tf

from utils.utils import create_directory
from models.constants import ArchitectureTests


class Executor:
    AUTOTUNE = tf.data.experimental.AUTOTUNE

    def __init__(self, data_config, batch_size=128, epochs=1, lr=0.001, output_path='results'):
        self.data_name = None
        self.num_classes = None
        self.input_shape = None
        self.split = None
        self.levels = list()
        self.level = .0
        self.arch_type = None
        self.data_config = data_config
        self.batch_size = batch_size
        self.epochs = epochs
        self.lr = lr
        self.output_path = output_path
        self.result_path = None
        self.test_type = 'base'

    def execute(self):

        summary = list()

        for data, config in self.data_config.items():
            self.clear()
            for level in self.levels:
                for arch_type in ArchitectureTests.ARCH_TYPES:

                    self.data_name, self.num_classes, self.input_shape, self.split = config
                    self.arch_type = arch_type
                    self.level = level

                    if self.avoid_fit():
                        continue

                    self.result_path = '{}/{}_results'.format(self.output_path, self.data_name)
                    create_directory(self.result_path)

                    data = self.get_data()
                    current, peak, exec_time = self.process(data)

                    memory_data = {
                        'dataset': self.data_name, 'Arch': self.arch_type, 'Level': self.level,
                        'current_mem': current / 10 ** 6, 'peak_mem': peak / 10 ** 6, 'time': exec_time
                    }
                    summary.append(memory_data)
        df_summary = pd.DataFrame(summary)
        df_summary.to_csv('{}/{}_summary.csv'.format(self.output_path, self.test_type))

    def get_data(self):
        raise NotImplementedError('subclasses must implement get_data()')

    def process(self, data):
        raise NotImplementedError('subclasses must implement process()')

    def clear(self):  # Dummy method
        pass

    def set_level(self, new_level):
        self.level = new_level

    def avoid_fit(self):
        avoid = True
        avail_small_dataset = self.data_name in ArchitectureTests.SMALL_SIZE_DATASETS and \
                              self.arch_type in ArchitectureTests.AVAIL_SMALL_ARCHS
        avail_mid_dataset = self.data_name in ArchitectureTests.MID_SIZE_DATASETS and \
                            self.arch_type in ArchitectureTests.AVAIL_MID_ARCHS
        avail_large_dataset = self.data_name in ArchitectureTests.LARGE_SIZE_DATASETS and \
                              self.arch_type in ArchitectureTests.AVAIL_LARGE_ARCHS
        if avail_small_dataset or avail_mid_dataset or avail_large_dataset:
            avoid = False
        return avoid

    def format_image(self, image, label):
        image = tf.image.resize(image, self.input_shape[:2])
        image = tf.cast(image, tf.float32)
        image = image / 255.
        return image, label

    def change_brightness(self, image, label):
        image = tf.image.adjust_brightness(image, self.level)
        return image, label

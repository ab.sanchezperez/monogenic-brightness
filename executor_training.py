#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import os
import gc
import time
import pandas as pd
import tracemalloc
import tensorflow as tf
import tensorflow_datasets as tfds

from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import History
from callbacks.callbackfactory import CallbackFactory
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import SparseCategoricalCrossentropy

from os.path import join
from executor import Executor
from models.modelfactory import ModelFactory

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'


class TrainExecutor(Executor):

    checkpoint_config = dict(
        monitor='val_accuracy',
        verbose=1,
        save_best_only=True,
        save_weights_only=True,
        mode='auto',
        save_freq='epoch'
    )

    def __init__(self, data_config, batch_size, epochs, lr, output_path):
        super(TrainExecutor, self).__init__(data_config, batch_size, epochs, lr, output_path)
        self.levels = [.0]
        self.test_type = 'train'

    def get_data(self,):
        (data_train, data_validation, _) = tfds.load(name=self.data_name, split=self.split, as_supervised=True)
        train = data_train.shuffle(1000).map(self.change_brightness)
        validation = data_validation.map(self.change_brightness)
        train = train.map(self.format_image).batch(self.batch_size).prefetch(self.AUTOTUNE)
        validation = validation.map(self.format_image).batch(self.batch_size).prefetch(self.AUTOTUNE)
        return train, validation

    def process(self, data):
        print('\nData name [{}] - Level [{}] - Test type [{}]'.format(
            self.data_name, self.level, self.arch_type)
        )
        train, validation = data
        model = ModelFactory.instance(arch_type=self.arch_type,
                                      input_shape=self.input_shape,
                                      num_classes=self.num_classes,
                                      data_name=self.data_name)
        print(model.summary())
        model.compile(optimizer=Adam(lr=self.lr), loss=SparseCategoricalCrossentropy(), metrics=['accuracy'])
        execution_name = '{}-{}'.format(self.data_name, self.arch_type)
        checkpoint = ModelCheckpoint(join(self.result_path, execution_name + '.h5'), **self.checkpoint_config)
        m6_callback = CallbackFactory.instance(model=model, data_name=self.data_name)
        print('Model name: {}'.format(execution_name))

        tracemalloc.start()
        start_time = time.time()

        history = model.fit(
            train, epochs=self.epochs, validation_data=validation,
            callbacks=[m6_callback, checkpoint, History()]
        )
        current, peak = tracemalloc.get_traced_memory()
        tracemalloc.stop()
        exec_time = time.time() - start_time

        df_history = pd.DataFrame(history.history)
        df_history.to_csv(join(self.result_path, 'history_{}.csv').format(execution_name), index=False)
        weights = m6_callback.get_weights()
        if weights.any():
            df = pd.DataFrame(weights)
            df.to_csv(join(self.result_path, 'weights_{}.csv'.format(execution_name)), index=False)
        del model
        tf.keras.backend.clear_session()
        gc.collect()
        return current, peak, exec_time

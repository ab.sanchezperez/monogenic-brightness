#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import gc
import os
import time
import pandas as pd
import tracemalloc
import tensorflow as tf
import tensorflow_datasets as tfds

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from os.path import join
from executor import Executor
from models.modelfactory import ModelFactory


class TestExecutor(Executor):

    def __init__(self, data_config, batch_size, epochs, lr, output_path):
        super(TestExecutor, self).__init__(data_config, batch_size, epochs, lr, output_path)
        self.evaluation = [['Model', 'Level', 'Test', 'Test_acc', 'Test_loss']]
        self.levels = [.0, .3, .4, .5]
        self.test_type = 'test'

    def get_data(self):
        (_, _, data_test) = tfds.load(name=self.data_name, split=self.split, as_supervised=True)
        test = data_test.map(self.change_brightness).map(self.format_image).batch(self.batch_size).prefetch(self.AUTOTUNE)
        return test

    def process(self, data):
        model_names = self.get_model_names(sorted(os.listdir(self.result_path)))
        current = peak = exec_time = 0
        for model_name in model_names:
            test_type, _ = os.path.splitext(model_name.split('-')[1])
            data_name = model_name.split('-')[0]
            if self.data_name == data_name and self.arch_type == test_type:
                model = ModelFactory.instance(arch_type=self.arch_type,
                                              input_shape=self.input_shape,
                                              num_classes=self.num_classes,
                                              data_name=self.data_name)
                model.load_weights(join(self.result_path, model_name))
                model.compile(optimizer=Adam(lr=self.lr), loss=SparseCategoricalCrossentropy(), metrics=['accuracy'])
                # print(model.summary())
                tracemalloc.start()
                start_time = time.time()

                test_loss, test_acc = model.evaluate(data, verbose=0)

                current, peak = tracemalloc.get_traced_memory()
                tracemalloc.stop()
                exec_time = time.time() - start_time

                self.evaluation.append([
                    model_name, str(self.level), self.arch_type, str(test_acc), str(test_loss)]
                )
                print('\nModel: [{}] - accuracy [{:.3f}] - loss [{:.3f}] - Level [{}]'.format(
                    model_name, test_acc, test_loss, self.level)
                )
                del model
                tf.keras.backend.clear_session()
                gc.collect()
        df = pd.DataFrame(self.get_evaluation())
        df.to_csv(join('{}', '{}_results.csv').format(self.output_path, self.data_name), index=False, header=False)
        return current, peak, exec_time

    def get_model_names(self, model_names):
        models = list()
        for name in model_names:
            if self.data_name in name and name.endswith('.h5'):
                models.append(name)
        return models

    def get_evaluation(self):
        return self.evaluation

    def clear(self):
        self.evaluation.clear()
        self.evaluation = [['Model', 'Level', 'Test', 'Test_acc', 'Test_loss']]

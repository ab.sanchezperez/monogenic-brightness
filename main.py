#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import argparse

from executor_test import TestExecutor
from executor_training import TrainExecutor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Monogenic Layer')
    parser.add_argument('-b', '--batch-size', default=128, required=False, type=int, help='Batch size')
    parser.add_argument('-e', '--epochs', default=1, required=False, type=int, help='Epochs')
    parser.add_argument('-l', '--learning-rate', default=0.001, type=float, required=False, help='Learning rate')
    parser.add_argument('-r', '--output-path', default='results', type=str, required=False, help='Results path')
    args = parser.parse_args()

    data_config = {
        # Dataset name | Classes | Image size | Split size
        'mnist': ('mnist', 10, (28, 28, 1), ['train[:80%]', 'train[80%:]', 'test']),
        'fashion_mnist': ('fashion_mnist', 10, (28, 28, 1), ['train[:80%]', 'train[80%:]', 'test']),
        'cifar10': ('cifar10', 10, (32, 32, 3), ['train[:80%]', 'train[80%:]', 'test']),
        'cats_vs_dogs': ('cats_vs_dogs', 2, (150, 150, 3), ['train[:70%]', 'train[70%:85%]', 'train[85%:]'])
    }

    trainer = TrainExecutor(
        data_config=data_config, batch_size=args.batch_size, epochs=args.epochs, lr=args.learning_rate,
        output_path=args.output_path
    )
    trainer.execute()
    tester = TestExecutor(
        data_config=data_config, batch_size=args.batch_size, epochs=args.epochs, lr=args.learning_rate,
        output_path=args.output_path
    )
    tester.execute()

    print('Done!')

#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"

import numpy as np

from tensorflow.keras.callbacks import Callback


class MonogenicCallback(Callback):
    
    def __init__(self, model):
        super(MonogenicCallback).__init__()
        self.weights = list()
        self.layer = None
        try:
            self.layer = model.get_layer('m6')
            wl_1, c_1, wl_2 = self.layer.get_weights()
            self.weights.append([0, wl_1, c_1, wl_2])
        except:
            print("[Callback] - M18 layer not found. Discarding monogenic callback for this model.")

    def on_epoch_end(self, epoch, logs=None):
        if self.layer is not None:
            wl_1, c_1 = self.layer.get_weights()
            self.weights.append([epoch+1, wl_1, c_1])

    def get_weights(self):
        return np.array(self.weights, dtype=np.float32)


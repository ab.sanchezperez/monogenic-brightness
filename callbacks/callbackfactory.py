#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


from models.constants import ArchitectureTests


class CallbackFactory:

    @staticmethod
    def instance(model, data_name):
        if data_name in ArchitectureTests.SMALL_SIZE_DATASETS:
            from callbacks.m6callback import MonogenicCallback
            return MonogenicCallback(model=model)
        else:
            from callbacks.m18callback import MonogenicCallback
            return MonogenicCallback(model=model)


#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Abraham Sanchez", "E. Ulises Moya", "Sebastia Xambo", "Baowei Fei", "Ulises Cortes"]
__copyright__ = "Copyright 2021, Gobierno de Jalisco"
__credits__ = ["Abraham Sanchez", "E. Ulises Moya"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Abraham Sanchez", "E. Ulises Moya"]
__email__ = "abraham.sanchez@jalisco.gob.mx"
__status__ = "Development"


import os
import tensorflow as tf

from tensorflow.keras.initializers import Initializer
from tensorflow.keras.layers import Layer

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'


class M6(Layer):

    def __init__(self, wl=1., c=.33, **kwargs):
        super(M6, self).__init__(**kwargs)
        self.wl_1 = self.add_weight(name='wl_1', shape=(1,), initializer=Constant(value=wl))
        self.c_1 = self.add_weight(name='c_1', shape=(1,), initializer=Constant(value=c))
        self.wl_2 = self.add_weight(name='wl_2', shape=(1,), initializer=Constant(value=wl))
        self.c_2 = self.add_weight(name='c_2', shape=(1,), initializer=Constant(value=c))
        self.wl_3 = self.add_weight(name='wl_3', shape=(1,), initializer=Constant(value=wl))
        self.c_3 = self.add_weight(name='c_3', shape=(1,), initializer=Constant(value=c))

    def call(self, inputs, **kwargs):
        _, cols, rows, channels = inputs.shape
        if channels != 3:
            raise ValueError('Monogenic layer only supports 3 channels.')
        channel_1 = inputs[..., 0]
        channel_2 = inputs[..., 1]
        channel_3 = inputs[..., 2]
        mr = self.monogenic_scale(cols=cols, rows=rows, wl=self.wl_1, c=self.c_1)
        mg = self.monogenic_scale(cols=cols, rows=rows, wl=self.wl_2, c=self.c_2)
        mb = self.monogenic_scale(cols=cols, rows=rows, wl=self.wl_3, c=self.c_3)
        tensor_1 = self.compute_monogenic(x=channel_1, m=mr)
        tensor_2 = self.compute_monogenic(x=channel_2, m=mg)
        tensor_3 = self.compute_monogenic(x=channel_3, m=mb)
        tensor = tf.concat([tensor_1, tensor_2, tensor_3], axis=-1)
        return tensor

    def get_config(self):
        config = super().get_config().copy()
        config.update({'wl_1': self.wl_1, 'c_1': self.c_1,
                       'wl_2': self.wl_2, 'c_2': self.c_2,
                       'wl_3': self.wl_3, 'c_3': self.c_3})
        return config

    def compute_monogenic(self, x, m):
        im = tf.signal.fft3d(tf.cast(x, tf.complex64))
        imf = im * m[..., 0]
        imh1 = im * m[..., 1]
        imh2 = im * m[..., 2]
        f = tf.math.real(tf.signal.ifft3d(imf))
        h1 = tf.math.real(tf.signal.ifft3d(imh1))
        h2 = tf.math.real(tf.signal.ifft3d(imh2))
        ori = tf.atan(tf.math.divide_no_nan(-h2, h1))
        fr = tf.sqrt(h1 ** 2 + h2 ** 2) + 0.001
        ft = tf.atan2(f, fr)
        ones = tf.ones_like(x)
        fts = self.scale_max_min(ft)
        frs = self.scale_max_min(fr)
        oris = self.scale_max_min(ori)
        hsv_tensor_v = tf.stack((fts, frs, ones), axis=-1)
        rgb_tensor_v = self.hsv_to_rgb(hsv_tensor_v)
        hsv_tensor_o = tf.stack((oris, frs, ones), axis=-1)
        rgb_tensor_o = self.hsv_to_rgb(hsv_tensor_o)
        rgb_tensor = tf.concat([rgb_tensor_o, rgb_tensor_v], axis=-1)
        return rgb_tensor

    @classmethod
    def hsv_to_rgb(cls, tensor):
        h = tensor[..., 0]
        s = tensor[..., 1]
        v = tensor[..., 2]
        c = s * v
        m = v - c
        dh = h * 6
        h_category = tf.cast(dh, tf.int32)
        fmodu = dh % 2
        x = c * (1 - tf.abs(fmodu - 1))
        component_shape = tf.shape(tensor)[:-1]
        dtype = tensor.dtype
        rr = tf.zeros(component_shape, dtype=dtype)
        gg = tf.zeros(component_shape, dtype=dtype)
        bb = tf.zeros(component_shape, dtype=dtype)
        h0 = tf.equal(h_category, 0)
        rr = tf.where(h0, c, rr)
        gg = tf.where(h0, x, gg)
        h1 = tf.equal(h_category, 1)
        rr = tf.where(h1, x, rr)
        gg = tf.where(h1, c, gg)
        h2 = tf.equal(h_category, 2)
        gg = tf.where(h2, c, gg)
        bb = tf.where(h2, x, bb)
        h3 = tf.equal(h_category, 3)
        gg = tf.where(h3, x, gg)
        bb = tf.where(h3, c, bb)
        h4 = tf.equal(h_category, 4)
        rr = tf.where(h4, x, rr)
        bb = tf.where(h4, c, bb)
        h5 = tf.equal(h_category, 5)
        rr = tf.where(h5, c, rr)
        bb = tf.where(h5, x, bb)
        r = rr + m
        g = gg + m
        b = bb + m
        return tf.stack([r, g, b], axis=-1)

    @classmethod
    def mesh_range(cls, size):
        if len(size) == 1:
            rows = cols = size
        else:
            rows, cols = size
        if cols % 2:
            x_values = tf.range(-(cols - 1) / 2., ((cols - 1) / 2.) + 1) / float(cols - 1)
        else:
            x_values = tf.range(-cols / 2., cols / 2.) / float(cols)
        if rows % 2:
            y_values = tf.range(-(rows - 1) / 2., ((rows - 1) / 2.) + 1) / float(rows - 1)
        else:
            y_values = tf.range(-rows / 2., rows / 2.) / float(rows)
        return tf.meshgrid(x_values, y_values)

    def low_pass_filter(self, size, cutoff, n):
        x, y = self.mesh_range(size)
        radius = tf.sqrt(x * x + y * y)
        lpf = tf.cast(tf.signal.ifftshift(1. / (1. + (radius / cutoff) ** (2. * n))), tf.complex64)
        return lpf

    def meshs(self, size):
        x, y = self.mesh_range(size)
        radius = tf.cast(tf.signal.ifftshift(tf.sqrt(x * x + y * y)), tf.complex64)
        x = tf.cast(tf.signal.ifftshift(x), tf.complex64)
        y = tf.cast(tf.signal.ifftshift(y), tf.complex64)
        return x, y, radius

    def riesz_trans(self, cols, rows):
        u1, u2, qs = self.meshs((rows, cols))
        qs = tf.cast(tf.sqrt(u1 * u1 + u2 * u2), tf.complex64)
        indices = tf.constant([[0, 0]])
        updates = tf.constant([1.], tf.complex64)
        q = tf.tensor_scatter_nd_update(qs, indices, updates)
        h1 = (1j * u1) / q
        h2 = (1j * u2) / q
        return h1, h2

    @classmethod
    def scale_max_min(cls, x):
        x_min = tf.reduce_min(x, axis=(1, 2), keepdims=True)
        x_max = tf.reduce_max(x, axis=(1, 2), keepdims=True)
        scale = tf.math.divide_no_nan(
            tf.subtract(x, x_min),
            tf.subtract(x_max, x_min)
        )
        return scale

    def log_gabor_scale(self, cols, rows, wl, c):
        u1, u2, rad_f = self.meshs((rows, cols))
        indices = tf.constant([[0, 0]])
        updates = tf.constant([1.], tf.complex64)
        radius = tf.tensor_scatter_nd_update(rad_f, indices, updates)
        lp = self.low_pass_filter((rows, cols), .45, 15)
        log_gabor_denominator = tf.cast(2. * tf.math.log(c) ** 2., tf.complex64)
        fo = tf.math.divide_no_nan(tf.constant(1., dtype=tf.float32), wl)
        log_rad_over_fo = tf.math.log(tf.math.divide_no_nan(radius, tf.cast(fo, tf.complex64)))
        log_gabor = tf.exp(tf.math.divide_no_nan(-(log_rad_over_fo * log_rad_over_fo), log_gabor_denominator))
        log_gabor = (lp * log_gabor)
        return log_gabor

    def monogenic_scale(self, cols, rows, wl, c):
        h1, h2 = self.riesz_trans(cols, rows)
        lg = self.log_gabor_scale(cols, rows, wl, c)
        lg_h1 = lg * h1
        lg_h2 = lg * h2
        m = tf.stack([lg, lg_h1, lg_h2], axis=-1)
        return m


class Constant(Initializer):

    def __init__(self, value):
        self.value = value

    def __call__(self, shape, dtype=tf.float32):
        return tf.Variable([self.value], dtype=dtype)
